import {
    Card,
    Box,
    TextField,
    Typography,
    Button
} from '@mui/material';
import InputAdornment from "@material-ui/core/InputAdornment";
import React from 'react';
import {Link} from 'react-router-dom';
import {Avatar} from '@mui/material';
import laptop from "../Images/laptop.png"
import Email from '@material-ui/icons/Email';
import Lock from '@material-ui/icons/Lock';


const Login = () => {
    return (
        <Box sx={{
            background: 'linear-gradient(90deg, rgba(67,88,203,1) 32%, rgba(200,81,189,1) 72%);',
            height: '100vh',
            width: '100vw',
        }}>
            <Card sx={{
                height: '50vh',
                width: '50vw',
                position: 'absolute',
                top: '50%',
                left: '50%',
                transform: 'translate(-50%, -50%)',
                borderRadius: '20px',
                display: 'flex'
            }}>
                <Box sx={{width: '50%', height: '70%'}}>
                    <Avatar alt='profile-picture' src={laptop}
                            variant='rounded' sx={{

                        height: '10rem',
                        width: '10rem',
                        position: 'absolute',
                        top: '50%',
                        left: '25%',
                        transform: 'translate(-50%, -50%)',
                    }}/>
                </Box>
                <Box sx={{width: '50%', height: '100%', position: 'relative'}}>
                    <Typography variant="h4" color="black"
                                sx={{
                                    margin: '2rem auto',
                                    textAlign: 'center'
                                }}>
                        User Login
                    </Typography>
                    <TextField id="outlined-basic" label="Email ID"
                               variant="outlined" InputProps={{
                        startAdornment: (
                            <InputAdornment position="start">
                                <Email/>
                            </InputAdornment>
                        )
                    }}
                               sx={{
                                   display: 'block',
                                   margin: '1rem 25%',
                                   width: '12rem'
                               }}
                    />
                    <TextField id="outlined-basic" label="Password"
                               variant="outlined" InputProps={{
                        startAdornment: (
                            <InputAdornment position="start">
                                <Lock/>
                            </InputAdornment>
                        )
                    }} sx={{display: 'block', margin: '1rem 25%', width: '12rem'}}
                    />
                    <Button variant="contained" color="success" sx={{
                        borderRadius: 28,
                        width: '14rem',
                        position: 'absolute',
                        left: '50%',
                        transform: 'translateX(-50%)',
                        display: 'block'
                    }}>Login</Button>
                    <Box>
                    <Typography
                        sx={{
                            marginTop: '5rem',
                            textAlign: 'center',
                        }}>
                        <Link to='/' style={{
                            textDecoration: 'none',
                            color: 'grey'
                        }}>Forgot Username/Password?</Link>
                    </Typography>
                    </Box>
                </Box>
            </Card>
        </Box>
    );
};

export default Login