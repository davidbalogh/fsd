import React, {useContext, useRef, useState} from 'react';
import {Button, Grid, Card, CardMedia, Box} from '@mui/material';
import {LoginContext} from "../Context/LoginContext";
import axios from "axios";
import useLocalStorage from "../hooks/useLocalStorage";

const SIZE = 28;

const Picture = () => {

    const isLoggedIn = useContext(LoginContext);

    const [img, setImg] = useState("");
    const [predictions, setPredictions] = useLocalStorage("images", JSON.stringify([]));
    const inputFile = useRef<HTMLInputElement>(null);

    const openFileDialog = () => {
        inputFile?.current?.click();
    };

    const onImgChange = (e: React.ChangeEvent<any>) => {

        let file = e.target.files[0];

        if (file) {
            var image = new Image();
            image.src = window.URL.createObjectURL(file);
            image.onload = () => {
                if (image.width === SIZE && image.height === SIZE) {
                    setImg(URL.createObjectURL(e.target.files[0]));
                    let data = new FormData();
                    data.append("image", file);
                    axios
                        .post("http://localhost:3001/evaluate", data, {
                            headers: {
                                'Content-Type': 'multipart/form-data'
                            }
                        })
                        .then((response) => {
                            setPredictions((old: any) => [...old,
                                {
                                    "name": file.name,
                                    "size": `${image.width} x ${image.height}`,
                                    "pred": response.data[0]
                                }
                            ]);

                        }).catch(error => {});
                } else {
                    alert(`Invalid size.`);
                }
            }
        }


    };

    if (isLoggedIn) {
        return (
            <Box sx={{
                display: 'flex',
                backgroundColor: '#C0C0C0',
                height: '100vh',
                flexWrap: 'wrap',
            }}>
                <Button variant="contained" component="span"
                        sx={{
                            backgroundColor: '#999999',
                            color: '#000000',
                            position: 'absolute',
                            right: 50,
                            top: 50
                        }}
                        onClick={openFileDialog}>
                    Upload
                </Button>
                <Grid
                    container spacing={0}
                    direction="column"
                    alignItems="center"
                    justifyContent="center"
                >
                    <Card sx={{height: 500, width: 800}}>
                        <CardMedia style={{ textAlign: "center" }}>
                            <img
                                style={{
                                    width: "auto",
                                    height: "auto",
                                    maxHeight: "100%",
                                    maxWidth: "100%",
                                }}
                                alt="img"
                                src={img}
                            />
                        </CardMedia>

                    </Card>
                    <input
                        type="file"
                        id="file"
                        ref={inputFile}
                        onChange={onImgChange}
                        style={{ display: "none" }}
                    />


                </Grid>
            </Box>
        )
    }
    else {
        return (
            <div>YOU ARE NOT LOGGED IN!</div>
        );
    }
};

export default Picture;