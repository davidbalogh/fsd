import React, { useContext } from 'react';
import { Link } from 'react-router-dom';
import { LoginContext } from '../Context/LoginContext';
import Box from '@mui/material/Box';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';

export class ImageResult {
	name: string;
	size: string;
	pred: number;
	downloadLink: string;

	constructor(
		name: string,
		size: string,
		recognitionResult: number,
		downloadLink: string,
	) {
		this.name = name;
		this.size = size;
		this.pred = recognitionResult;
		this.downloadLink = downloadLink;
	}
}

interface Data {
	name: string;
	size: number;
	recognitionResult: number;
	downloadLink: string;
}

export interface DateData {
	start_date: string;
	end_date: string;
}

type HeadCell = {
	disablePadding: boolean;
	id: keyof Data;
	label: string;
	numeric: boolean;
}

type HeadCellDate = {
	disablePadding: boolean;
	id: keyof DateData;
	label: string;
	numeric: boolean;
}

const headCells: readonly HeadCell[] = [
	{
		id: 'name',
		numeric: false,
		disablePadding: true,
		label: 'Name of image',
	},
	{
		id: 'size',
		numeric: true,
		disablePadding: false,
		label: 'Size (Pixels)',
	},
	{
		id: 'recognitionResult',
		numeric: true,
		disablePadding: false,
		label: 'Recognition Result',
	},
	{
		id: 'downloadLink',
		numeric: true,
		disablePadding: false,
		label: 'Download Link',
	},
];

const headCellsDate: readonly HeadCellDate[] = [
	{
		id: 'start_date',
		numeric: true,
		disablePadding: false,
		label: 'start_date',
	},
	{
		id: 'end_date',
		numeric: true,
		disablePadding: false,
		label: 'end_date',
	},
];

interface EnhancedTableProps {
	isDateTable?: boolean;
}

function EnhancedTableHead(props: EnhancedTableProps) {
	const { isDateTable } = props;
	const headCellsChoice = isDateTable ? headCellsDate : headCells;

	return (
		<TableHead>
			<TableRow>
				{headCellsChoice.map((headCell) => (
					<TableCell
						key={headCell.id}
						align={headCell.numeric ? 'right' : 'center'}
						padding={headCell.disablePadding ? 'normal' : 'normal'}
					>
						{headCell.label}
					</TableCell>
				))}
			</TableRow>
		</TableHead>
	);
}

type EnhancedTableType = {
	isDateTable?: boolean
	dateTableData?: DateData[]
}

export default function EnhancedTable(props: EnhancedTableType) {
	const newImageRows: Array<ImageResult> = JSON.parse(localStorage.getItem('images') ?? '');

	const isDateTable = props.isDateTable;
	const newRows = isDateTable ? props.dateTableData : newImageRows || [""];

	const isLoggedIn = useContext(LoginContext);
	if ( isLoggedIn && newRows) {
		return (
				<Box sx={{ width: '100%' }}>
					<Paper sx={{ width: '100%', mb: 2 }}>
						<TableContainer>
							<Table
								sx={{ minWidth: 750 }}
								aria-labelledby="tableTitle"
							>
								<EnhancedTableHead
									isDateTable={props.isDateTable}
								/>
								<TableBody>
									{newRows.map((row, index) => {
										const labelId = `enhanced-table-checkbox-${index}`;
										return (
											!isDateTable
												? <TableRow
													role="checkbox"
													tabIndex={-1}
												>
													<TableCell component="th" id={labelId} scope="row"
															   padding="none" align="center">{(row as ImageResult).name}</TableCell>
													<TableCell align="right">{(row as ImageResult).size}</TableCell>
													<TableCell align="right">{(row as ImageResult).pred}</TableCell>
													<TableCell align="right">
														<Link target="_blank" to={(row as ImageResult).downloadLink}
															  style={{ color: 'blue', textDecoration: 'none' }}>Click here to
															download {(row as ImageResult).name}</Link>
													</TableCell>
												</TableRow>
												: <TableRow
													role="checkbox"
													tabIndex={-1}
													key={(row as ImageResult).name}
												>
													<TableCell align="right"> {(row as DateData).start_date}</TableCell>
													<TableCell align="right"> {(row as DateData).end_date}</TableCell>
												</TableRow>
										);
									})}
								</TableBody>
							</Table>
						</TableContainer>
					</Paper>
				</Box>
		);
	} else {
		return (
			<div>{!isLoggedIn ? "NOT LOGGED IN" : "NO DATA"}</div>
		);
	}
}