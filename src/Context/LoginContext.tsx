import React, { createContext, useState } from "react";

export const LoginContext = createContext<boolean>(false);

export const LoginProvider:React.FC = (props) => {
    const [isLoggedIn, setIsLoggedIn] = useState<boolean>(true);

    return (
        <LoginContext.Provider value={isLoggedIn}>
            {props.children}
        </LoginContext.Provider>
    );
};