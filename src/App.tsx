import React from 'react';
import './App.css';
import HomePage from "./Components/HomePage";
import {BrowserRouter as Router, Route, Switch} from "react-router-dom"
import Login from "./Components/Login";
import Tables from "./Components/Tables";
import Picture from "./Components/Picture";
import Calendar from "./Components/Calendar";
import {LoginProvider} from "./Context/LoginContext";

function App() {
    return (
      <LoginProvider>
      <Router>
        <div>
          <Switch>
            <Route path='/' exact component={HomePage}/>
            <Route path='/login' exact component={Login}/>
            <Route path='/tables' exact component={Tables}/>
            <Route path='/picture' exact component={Picture}/>
            <Route path='/calendar' exact component={Calendar}/>
          </Switch>
        </div>
      </Router>
      </LoginProvider>
  );
}

export default App;