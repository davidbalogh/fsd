import axios, {AxiosRequestConfig} from 'axios';

const instance = axios.create({
    baseURL: process.env.API_BACKEND,
    timeout: 1000,
    headers: {'X-Custom-Header': 'foobar'}
});


instance.interceptors.request.use(
    function (config: AxiosRequestConfig) {
        const token = localStorage.getItem("token");
        if (token && config.headers) {
            config.headers.Authorization = `Bearer ${token}`;
        }

        return config;
    },
    function (error) {
        return Promise.reject(error);
    }
);

instance.interceptors.response.use(
    (res) => res,
    function (error) {
        console.log(error.response);

        if (error.response.status === 401) {
            alert(error.response.config.url);
        }
        return Promise.reject(error);
    }
);

export default instance;

